// +build darwin linux windows

package res

import (
	"encoding/binary"

	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/gl"
)

type imbutton struct {
	*cell
	texture gl.Buffer
}

// IM button widget
func Button(g gl.Context, id string, x, y float32) bool {
	//TODO abstract away GL platform dependencies
	//     AND related pkg-globals
	b, ok := widgets[id]
	if !ok {
		b = newIMButton(g, id, x, y)
		widgets[id] = b
	}
	//todo Else re-position button as specified x,y ?
	//todo expect that the type fetched is a 'button'

	// Check whether the button should be hot
	if regionhit(x, y, Btnwpx, Btnhpx) {
		UI.hotitem = id
		if UI.activeitem == "none" && UI.mousedown {
			UI.activeitem = id
		}
	}

	// Assert the widget is type 'imbutton'
	button, ok := b.(*imbutton)
	if !ok {
		return false
	}

	if UI.hotitem == id {
		if UI.activeitem == id {
			// Button is highlighted and activated
			button.rval = BtnRa
			button.gval = BtnGa
			button.bval = BtnBa
		} else {
			// Button is highlighted
			button.rval = BtnRh
			button.gval = BtnGh
			button.bval = BtnBh
		}
	} else {
		button.rval = BtnR
		button.gval = BtnG
		button.bval = BtnB
	}

	b.draw(g)

	// Button click registered
	if UI.mousedown &&
		UI.hotitem == id &&
		UI.activeitem == id {
		return true
	}
	return false
}

func (b imbutton) draw(g gl.Context) {
	b.cell.draw(g)

	// We also draw the action symbol "texture"
	// onto the button's face

	//todo static texture color, for now
	g.Uniform4f(BtnClr, BtnRa, BtnGa, BtnBa, 1)

	g.BindBuffer(gl.ARRAY_BUFFER, b.texture)
	g.EnableVertexAttribArray(BtnPos)
	g.VertexAttribPointer(BtnPos, coordsPerVertex, gl.FLOAT, false, 0, 0)
	g.DrawArrays(gl.TRIANGLES, 0, vertexCount)
	g.DisableVertexAttribArray(BtnPos)
}

func (b imbutton) release(g gl.Context) {
	g.DeleteBuffer(b.texture)
	b.cell.release(g)
}

func newIMButton(g gl.Context, id string, x, y float32) *imbutton {
	w := &imbutton{
		cell: drawrect(g, x, y, Btnwpx, Btnhpx, BtnR, BtnG, BtnB),
	}
	w.texture = w.newBtnTexture(g, id)
	return w
}

func (b imbutton) newBtnTexture(g gl.Context, id string) gl.Buffer {
	var sz int
	var verts []float32
	switch id {
	case "dismissBtn":
		sz = len(square)
		verts = make([]float32, sz, sz)
		copy(verts, square)
	default:
		sz = len(triangle)
		verts = make([]float32, sz, sz)
		copy(verts, triangle)
	}

	var h float32 = Btnhpx * 0.80
	var w float32 = h
	var x float32 = b.x + (Btnwpx / 2) - w/2
	var y float32 = b.y + (Btnhpx / 2) - h/2

	// tri top lt
	verts[0] = normalizeX(x)
	verts[1] = normalizeY(y)
	// tri bot lt
	verts[3] = normalizeX(x)
	verts[4] = normalizeY(y + h)
	// tri bot rt
	verts[6] = normalizeX(x + w)
	verts[7] = normalizeY(y + h)

	if sz == len(square) {
		// tri top lt
		verts[9] = normalizeX(x)
		verts[10] = normalizeY(y)
		// tri top rt
		verts[12] = normalizeX(x + w)
		verts[13] = normalizeY(y)
		// tri bot rt
		verts[15] = normalizeX(x + w)
		verts[16] = normalizeY(y + h)
	}

	data := f32.Bytes(binary.LittleEndian, verts...)
	return makeVao(g, data)
}
