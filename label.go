// +build darwin linux windows

package res

import (
	"image"
	"image/color"

	////"github.com/golang/freetype/truetype"
	"golang.org/x/mobile/event/size"
	"golang.org/x/mobile/exp/gl/glutil"
	"golang.org/x/mobile/geom"
)

//TODO too global, refactor "Headerht" and id
func Label(sz size.Event, id int, title string, images *glutil.Images) {

	headerHeightPx := Headerht
	footerHeightPx := headerHeightPx
	fontsz := zoomFont(24)
	bodyht := UI.HeightPx - headerHeightPx - footerHeightPx
	drawBodyRow(sz, headerHeightPx, bodyht, title, fontsz, images)

}

func drawBodyRow(sz size.Event, startht int, rowht int, word string, fontsz float64, images *glutil.Images) {

	row := &TextSprite{
		text:            word,
		font:            Labelfont,
		widthPx:         UI.WidthPx,
		heightPx:        rowht,
		textColor:       image.White,
		backgroundColor: image.NewUniform(color.RGBA{0x00, 0x00, 0x00, 0xFF}),
		fontSize:        fontsz,
		xPt:             0,
		yPt:             PxToPt(startht),
	}
	row.Render(sz, images)
}

func PxToPt(sizePx int) geom.Pt {
	return geom.Pt(float32(sizePx) / UI.PixelsPerPt)
}

/*
// zoomHdrHt calculates 1/6th ratio of screen as hdr/ftr height
func zoomHdrHt(sz size.Event) int {
	scale := sz.HeightPx / 6
	Headerht = int(scale)
	return Headerht
}
*/

// zoomFont calculates base/688 ratio of screen as font size
func zoomFont(base int) float64 {
	scale := UI.HeightPx / 688
	return float64(scale * base)
}
