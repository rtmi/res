// +build darwin linux windows

//
// For reference see http://iki.fi/sol/imgui/
package res

import (
	"github.com/golang/freetype/truetype"
	"golang.org/x/mobile/gl"
)

type UIState struct {
	mouseX      float32
	mouseY      float32
	mousedown   bool
	hotitem     string
	activeitem  string
	WidthPx     int
	HeightPx    int
	PixelsPerPt float32
}

type Widget interface {
	draw(gl.Context)
	release(gl.Context)
}

// Unit element
// where drawable is the square Vertex Array Object
type cell struct {
	drawable gl.Buffer
	rval     float32
	gval     float32
	bval     float32
	x        float32
	y        float32
}

var (
	UI        *UIState
	Headerht  int
	Labelfont *truetype.Font
	BtnPos    gl.Attrib
	BtnClr    gl.Uniform
	widgets   map[string]Widget
)

// The "unit" rectangle
var square = []float32{
	-0.5, 0.5, 0.0, // tri top
	-0.5, -0.5, 0.0, // tri bottom lt
	0.5, -0.5, 0.0, // tri bottom rt

	-0.5, 0.5, 0.0, // tri top lt
	0.5, 0.5, 0.0, // tri top rt
	0.5, -0.5, 0.0, // tri bottom
}

// The "unit" triangle
var triangle = []float32{
	-0.5, 0.5, 0.0, // tri top
	-0.5, -0.5, 0.0, // tri bottom lt
	0.5, -0.5, 0.0, // tri bottom rt
}

const (
	coordsPerVertex = 3
	vertexCount     = 6
	Btnhpx          = 48
	Btnwpx          = 200
	BtnR            = 0.118
	BtnG            = 0.565
	BtnB            = 1.000
	BtnRa           = 0.902
	BtnGa           = 0.902
	BtnBa           = 0.980
	BtnRh           = 0.502
	BtnGh           = 0.502
	BtnBh           = 0.502
)
