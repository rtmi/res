# res
This is an exercise in learning immediate mode UI. 
 The first experiment is limited to a single button and toggle
 for a Gomobile app.


## Example
See the project 
 [BTC Rcv](https://github.com/patterns/btcrcv/)
 for example usage.


## Credits
IM GUI
 [dear imgui](https://github.com/ocornut/imgui/)
 [(LICENSE)](https://github.com/ocornut/imgui/blob/master/LICENSE.txt)

 and [ImGui library tutorial](http://iki.fi/sol/imgui/)

Luxi Mono is by [Bigelow & Holmes](https://www.fontsquirrel.com/fonts/luxi-mono)

Truetype font (TTF) rendering is from
 [Antoine Richard](https://github.com/antoine-richard/gomobile-text/)


