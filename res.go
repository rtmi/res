// +build darwin linux windows

//
// For reference see http://iki.fi/sol/imgui/
package res

import (
	"encoding/binary"

	"golang.org/x/mobile/exp/f32"
	"golang.org/x/mobile/gl"
)

// Prepare for IMGUI processing
func Prepare() {
	UI.hotitem = "none"
}

// Reset after IMGUI processed
func Finish() {
	if !UI.mousedown {
		UI.activeitem = "none"
	} else if UI.activeitem == "none" {
		UI.activeitem = ""
	}
}

// Hitbox check
func regionhit(x, y float32, w int, h int) bool {
	wval := float32(w)
	hval := float32(h)
	if UI.mouseX < x ||
		UI.mouseX > (x+wval) ||
		UI.mouseY < y ||
		UI.mouseY > (y+hval) {
		return false
	}
	return true
}

// drawrect is to construct a drawable rectangle
func drawrect(gcx gl.Context, x, y float32, w, h int, r, g, b float32) *cell {
	c := newCell(gcx, x, y, w, h)
	c.rval = r
	c.gval = g
	c.bval = b
	return c
}

// draw displays the underlying rectangle
func (c cell) draw(g gl.Context) {
	g.Uniform4f(BtnClr, c.rval, c.gval, c.bval, 1)

	g.BindBuffer(gl.ARRAY_BUFFER, c.drawable)
	g.EnableVertexAttribArray(BtnPos)
	g.VertexAttribPointer(BtnPos, coordsPerVertex, gl.FLOAT, false, 0, 0)
	g.DrawArrays(gl.TRIANGLES, 0, vertexCount)
	g.DisableVertexAttribArray(BtnPos)
}

// release frees the cell's resources
func (c cell) release(g gl.Context) {
	g.DeleteBuffer(c.drawable)
}

// Let's fix / lock the NW point of the unit rectangle
// and re-size in the south-east direction
func newCell(g gl.Context, x, y float32, w, h int) *cell {
	sz := len(square)
	verts := make([]float32, sz, sz)
	copy(verts, square)

	//todo what is the matrix transform to do this?
	// tri top lt
	verts[0] = normalizeX(x)
	verts[1] = normalizeY(y)
	// tri bot lt
	verts[3] = normalizeX(x)
	verts[4] = normalizeY(y + float32(h))
	// tri bot rt
	verts[6] = normalizeX(x + float32(w))
	verts[7] = normalizeY(y + float32(h))

	// tri top lt
	verts[9] = normalizeX(x)
	verts[10] = normalizeY(y)
	// tri top rt
	verts[12] = normalizeX(x + float32(w))
	verts[13] = normalizeY(y)
	// tri bot rt
	verts[15] = normalizeX(x + float32(w))
	verts[16] = normalizeY(y + float32(h))

	data := f32.Bytes(binary.LittleEndian, verts...)
	return &cell{
		drawable: makeVao(g, data),
		x:        x,
		y:        y,
	}
}

// see kylewbanks.com/blog/tutorial-opengl-with-golang-part-1-hello-opengl
func makeVao(g gl.Context, data []byte) gl.Buffer {
	var vbo gl.Buffer
	vbo = g.CreateBuffer()
	g.BindBuffer(gl.ARRAY_BUFFER, vbo)
	g.BufferData(gl.ARRAY_BUFFER, data, gl.STATIC_DRAW)
	return vbo
}

// Normalize screen X down to the -1 to +1 coordinates
func normalizeX(x float32) float32 {
	xcomponent := x / float32(UI.WidthPx)
	return (xcomponent * 2) - 1
}

// gl basics: https://webglfundamentals.org/webgl/lessons/webgl-2d-scale.html
// normalizing x,y : https://stackoverflow.com/q/22596275
// Normalize screen Y down to the -1 to +1 coordinates
func normalizeY(y float32) float32 {
	ycomponent := y / float32(UI.HeightPx)
	return (ycomponent * -2) + 1
}

func Touch(x float32, y float32, down bool) {
	UI.mouseX = x
	UI.mouseY = y
	UI.mousedown = down
}

func Dimensions(w int, h int, px float32) {
	UI.WidthPx = w
	UI.HeightPx = h
	UI.PixelsPerPt = px
	Headerht = h / 6
}

// Initialize is intended the initialization and zeros the state fields
func Initialize() (err error) {
	UI = &UIState{
		mouseX:     0,
		mouseY:     0,
		mousedown:  false,
		hotitem:    "none",
		activeitem: "none",
	}
	Labelfont, err = LoadBodyFont()
	widgets = make(map[string]Widget)
	return
}

func Release(g gl.Context) {
	var keys []string
	for k, v := range widgets {
		v.release(g)
		keys = append(keys, k)
	}
	for _, v := range keys {
		delete(widgets, v)
	}
}
